#!/usr/bin/env bash
LC_ALL=C

local_branch="$(git rev-parse --abbrev-ref HEAD)"

valid_branch_regex="^(story|feature|bugfix|hotfix)\/[a-zA-Z]+-[0-9]+.+"

message="Wrong branch name ti-chum. \n You must adhere to: $valid_branch_regex.."

if [[ ! $local_branch =~ $valid_branch_regex ]]
then
    echo "$message"
    exit 1
fi

exit 0